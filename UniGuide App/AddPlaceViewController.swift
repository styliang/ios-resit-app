//
//  AddPlaceViewController.swift
//  UniGuide App
//
//  Created by Giorgosstyliano on 7/13/17.
//  Copyright © 2017 Giorgosstyliano All rights reserved.
//

import UIKit

class AddPlaceViewController: UIViewController {
    var placesArray : [PlaceData] = [PlaceData]()
    
    @IBOutlet weak var informationTV: UITextView!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var placeNameTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        if ImportantPlacesViewController.selectedItem == -1{
            placeNameTF.text = ""
            contactTF.text = ""
            informationTV.text = ""
        }else{
            placesArray = DatabaseManager.shared.loadPlaces()
            placeNameTF.text = placesArray[ImportantPlacesViewController.selectedItem].placeName
            informationTV.text = placesArray[ImportantPlacesViewController.selectedItem].placeDescription
            contactTF.text = placesArray[ImportantPlacesViewController.selectedItem].placeContact
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        let place = PlaceData()
        print("place added is name \(placeNameTF.text!)")
        place.placeName = placeNameTF.text!
        place.placeContact = contactTF.text!
        place.placeDescription = informationTV.text!
        if ImportantPlacesViewController.selectedItem == -1 {
            let placeInserted = DatabaseManager.shared.insertPlace(place: place)
            print("Id of place is \(placeInserted.placeID)")
            if placeInserted.placeID > 0 {
                let alert = UIAlertController(title: "Place Added Successfully!", message: "\(placeInserted.placeName) Place Added Successfully!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Place Not Added!", message: "Place Not Added!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            place.placeID = placesArray[ImportantPlacesViewController.selectedItem].placeID
            let placeUpdated = DatabaseManager.shared.updatePlace(place: place)
            
            if placeUpdated.placeID > 0 {
                let alert = UIAlertController(title: "Place Updated Successfully!", message: "\(placeUpdated.placeName) Place Updated Successfully!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Place Not Updated!", message: "Place Not Updated!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
