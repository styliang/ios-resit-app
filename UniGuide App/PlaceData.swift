//
//  PlaceData.swift
//  UniGuide App
//
//  Created by Giorgosstyliano on 7/13/17.
//  Copyright © 2017 Giorgosstyliano All rights reserved.
//

import Foundation
class PlaceData{
    private var _placeID : Int = 0
    private var _placeName : String = ""
    private var _placeContact : String = ""
    private var _placeDescription : String = ""
    
    
    var placeID: Int {
        set { _placeID = newValue }
        get { return _placeID }
    }
    
    var placeName: String {
        set { _placeName = newValue }
        get { return _placeName }
    }
    
    var placeContact: String {
        set { _placeContact = newValue }
        get { return _placeContact }
    }
    
    var placeDescription: String {
        set { _placeDescription = newValue }
        get { return _placeDescription }
    }
    
}
