//
//  PlaceDetailViewController.swift
//  UniGuide App
//
//  Created by Giorgosstyliano on 7/13/17.
//  Copyright © 2017 Giorgosstyliano All rights reserved.
//

import UIKit

class PlaceDetailViewController: UIViewController {
    var placesArray : [PlaceData] = [PlaceData]()
    
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var placeNameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        placesArray = DatabaseManager.shared.loadPlaces()
        placeNameLbl.text = placesArray[ImportantPlacesViewController.selectedItem].placeName
        
        descriptionLbl.text = placesArray[ImportantPlacesViewController.selectedItem].placeDescription
        
        
        contactLbl.text = placesArray[ImportantPlacesViewController.selectedItem].placeContact
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
