//
//  StudentUnionViewController.swift
//  UniGuide App
//
//  Created by Giorgosstyliano on 7/13/17.
//  Copyright © 2017 Giorgosstyliano All rights reserved.
//

import UIKit
import WebKit

class StudentUnionViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://www.cusu.org")!
        webView.loadRequest(URLRequest(url: url))
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
