//
//  ImportantPlacesViewController.swift
//  UniGuide App
//
//  Created by Giorgosstyliano on 7/13/17.
//  Copyright © 2017 Giorgosstyliano All rights reserved.
//

import UIKit

class ImportantPlacesViewController: UIViewController {
    var placesArray : [PlaceData] = [PlaceData]()
    public static var selectedItem = -1
    static var selectedPlace = PlaceData()
    
    @IBOutlet weak var placeTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        placeTable.register(UINib(nibName: "ImportantPlaceTableViewCell", bundle: nil), forCellReuseIdentifier : "ImportantPlaceTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addNewPlaceBtnClicked(_ sender: Any) {
        ImportantPlacesViewController.selectedItem = -1
        performSegue(withIdentifier: "addInformation", sender: nil)
    }
    @IBAction func backBtnclicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "placeDetail"{
            if segue.destination is PlaceDetailViewController{
                
            }
        }else if segue.identifier == "addInformation"{
            if segue.destination is AddPlaceViewController{
                
            }
        }else if segue.identifier == "editInformation"{
            if segue.destination is AddPlaceViewController{
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        placesArray = [PlaceData]()
        
        placesArray = DatabaseManager.shared.loadPlaces()
        
        print(placesArray.count)
        if placesArray.count < 1{
            let alert = UIAlertController(title: "Places Not Found!", message: "Please Add A Place First.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            placeTable.reloadData()
        }
    }
    

}

extension ImportantPlacesViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImportantPlaceTableViewCell", for: indexPath) as! ImportantPlaceTableViewCell
        cell.placeNameLbl.text = placesArray[indexPath.row].placeName
        cell.placeDescription.text = placesArray[indexPath.row].placeContact
        
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self,action:#selector(editBtnClicked(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ImportantPlacesViewController.selectedItem = indexPath.row
        performSegue(withIdentifier: "placeDetail", sender: nil)
    }
    
    

    func editBtnClicked(sender:UIButton) {
        let buttonRow = sender.tag
        ImportantPlacesViewController.selectedItem = buttonRow
        performSegue(withIdentifier: "editInformation", sender: nil)
    }

}

