//
//  DBManager.swift
//  UniGuide App
//
//  Created by Adil Sher on 7/13/17.
//  Copyright © 2017 Adil Sher. All rights reserved.
//

import Foundation
import UIKit
import FMDB

class DatabaseManager: NSObject {
    static let shared: DatabaseManager = DatabaseManager()
    
    let databaseFileName = "uniquide.sqlite"
    
    var pathToDatabase: String!
    
    var database: FMDatabase!
    
    let field_PlaceID = "placeID"
    let field_PlaceName = "placeName"
    let field_PlaceDescription = "placeDescription"
    let field_placeContact = "placeContact"
    
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    //create database and tables
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createPlacesTableQuery = "create table places (\(field_PlaceID) integer primary key autoincrement not null, \(field_PlaceName) text not null, \(field_placeContact) text not null, \(field_PlaceDescription) text not null);"
                    
                    
                    do {
                        try database.executeUpdate(createPlacesTableQuery, values: nil)
                        
                        created = true
                        print("Database and tables created")
                    }
                    catch {
                        print("Could not create tables.")
                        print(error.localizedDescription)
                    }
                    
                    // At the end close the database.
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    //check db is open or not yet?
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }
    
    
    //insert driver in db
    func insertPlace(place : PlaceData)->PlaceData {
        
        if openDatabase() {
            var query = ""
            
            query = "insert into places (\(field_PlaceID), \(field_PlaceName), \(field_placeContact), \(field_PlaceDescription)) values (null, '\(place.placeName)', '\(place.placeContact)', '\(place.placeDescription)');"
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }else{
                let query2 = "SELECT * FROM places ORDER BY \(field_PlaceID) DESC LIMIT 1;"
                do {
                    let results = try database.executeQuery(query2, values: nil)
                    
                    if results.next() {
                        let place = PlaceData()
                        
                        place.placeID = Int(results.int(forColumn: field_PlaceID))
                        place.placeName = results.string(forColumn: field_PlaceName)
                        place.placeContact = results.string(forColumn: field_placeContact)
                        place.placeDescription = results.string(forColumn: field_PlaceDescription)
                        print("Place added is \(place.placeName)")
                        return place
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
                
                
            }
            print("Inserted")
            
        }
        database.close()
        return PlaceData()
    }
    
    //load all drivers from db
    func loadPlaces() -> [PlaceData]! {
        var placesArray: [PlaceData] = [PlaceData]()
        
        if openDatabase() {
            let query = "select * from places"
            
            do {
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    let place = PlaceData()
                    
                    place.placeID = Int(results.int(forColumn: field_PlaceID))
                    place.placeName = results.string(forColumn: field_PlaceName)
                    place.placeContact = results.string(forColumn: field_placeContact)
                    place.placeDescription = results.string(forColumn: field_PlaceDescription)
                    
                    placesArray.append(place)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        
        return placesArray
    }
    
    func updatePlace(place : PlaceData) -> PlaceData {
        if openDatabase() {
            let query = "update places set \(field_PlaceName) = '\(place.placeName)', \(field_placeContact) = '\(place.placeContact)', \(field_PlaceDescription) = '\(place.placeDescription)' where \(field_PlaceID) = '\(place.placeID)'"
            
            do {
                try database.executeUpdate(query, values: nil)
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return place
    }

}
